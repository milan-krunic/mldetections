import os
import argparse
import matplotlib.pyplot as plt

from PIL import Image
import numpy as np
import tqdm
import cv2
import pandas as pd

from util import *
from sampling import *

parser = argparse.ArgumentParser()
parser.add_argument('input_folder', type=str)
parser.add_argument('output_folder', type=str)
parser.add_argument('--defects', dest='defects', nargs='*')

args = parser.parse_args()
input_folder = args.input_folder
output_folder = args.output_folder
defects = args.defects

if defects is None or len(defects) == 0:
    defects = list(COLOR_MAP.keys())

window_width, window_height = 100, 100

# Create output directory if it does not exist.
if not os.path.exists(output_folder):
    os.mkdir(output_folder)

window_names = []
all_short_labels = []
all_full_labels = []

# Iterate through all of the image files in the input folder.
for jpg_file, png_file in tqdm.tqdm(list(get_image_pairs(input_folder))):

    # Load jpg and png image.
    jpg = load_jpg(jpg_file)
    png = load_png(png_file)

    try:
        windows, short_labels, full_labels = sample_windows(jpg, png)
    except ValueError:
        print ('No blade was found on image {}'.format(jpg_file))
        continue
    except AssertionError:
        print(jpg_file)

    n = 1
    for window, short_label, full_label in zip(windows, short_labels, full_labels):
        # Generate image name.
        file_path, ext = os.path.splitext(jpg_file)
        img_name = ''.join([os.path.basename(file_path), '_', str(n)])
        img_output_path = os.path.join(output_folder, img_name + '.' + ext)

        n += 1

        # Save image.
        plt.imsave(img_output_path, window, format='jpg', dpi=1)

        # Add metadata to a global array.
        window_names.append(img_name)
        all_short_labels.append(short_label)
        all_full_labels.append(full_label)

targets_df = pd.DataFrame({'Name': window_names, 'WantedDefect': all_short_labels, 'FindingCat': all_full_labels})
targets_df.to_csv(os.path.join(output_folder, 'targets.csv'), index=False)

print('Sampled {:.2f} % images with wanted defects.'.format(targets_df.WantedDefect.mean() * 100))
