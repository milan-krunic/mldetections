import os
import argparse
import shutil

import pandas as pd

parser = argparse.ArgumentParser()
parser.add_argument('first_folder', type=str)
parser.add_argument('second_folder', type=str)

args = parser.parse_args()
first_folder = args.first_folder
second_folder = args.second_folder

# Check if input and output folders exists.
if not os.path.exists(first_folder):
    raise ValueError('{} does not exist'.format(first_folder))

if not os.path.exists(second_folder):
    raise ValueError('{} does not exist'.format(second_folder))

# Load and merge targets.csv files from the two directories.
first_targets = pd.read_csv(os.path.join(first_folder, 'targets.csv'))
second_targets = pd.read_csv(os.path.join(second_folder, 'targets.csv'))

targets = pd.concat([first_targets, second_targets], join='inner', ignore_index=True)

assert first_targets.shape[0] + second_targets.shape[0] == targets.shape[0]

# Move images from second folder to first folder.
first_images = set([img_file for img_file in os.listdir(first_folder) if img_file.endswith('.jpg')])
second_images = [img_file for img_file in os.listdir(second_folder) if img_file.endswith('.jpg')]

print('Moving images')
for second_image in second_images:
    if second_image in first_images:
        print('Image with the same name as {} exists in first folder.'.format(second_image))
        continue

    shutil.move(os.path.join(second_folder, second_image), os.path.join(first_folder, second_image))

print('Moving targets file.')
targets.to_csv(os.path.join(first_folder, 'targets.csv'), index=False)

print('Deleting old targets file.')
os.remove(os.path.join(second_folder, 'targets.csv'))

print('Done')
