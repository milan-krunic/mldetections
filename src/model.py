import os
import tqdm

from collections import namedtuple
import tensorflow as tf
import numpy as np
import pandas as pd
from sklearn.preprocessing import OneHotEncoder
import cv2

from model_util import *
from util import *

#########################
### Data loading.
#########################
def get_labeled_images(folder, labels_file):
    img_files = [os.path.join(folder, img_file) for img_file in os.listdir(folder) if img_file.endswith('.jpg')]
    img_file_names = [os.path.basename(img_file).split('.')[0] for img_file in img_files]

    labels = pd.read_csv(labels_file).set_index('Name')

    labels = labels.WantedDefect.loc[img_file_names].astype(np.int0)
    labels = labels.values.reshape((-1, 1))
    labels = OneHotEncoder(sparse=False).fit_transform(labels)

    return img_files, labels

def get_input_queue(img_files, labels, name=None):
    img_files = tf.convert_to_tensor(img_files)
    labels = tf.convert_to_tensor(labels)
    input_producer = tf.train.slice_input_producer([img_files, labels],
                                                   #num_epochs=epochs,
                                                   name=name)

    return input_producer

def labeled_images_reader(img_files, labels, img_height, img_width, name=None):
    img_file, label = get_input_queue(img_files, labels, name=name)

    contents = tf.read_file(img_file)
    img = tf.image.decode_jpeg(contents, channels=3)
    img = tf.reshape(img, (img_height, img_width, 3))

    # Perform flips.
    img = tf.image.random_flip_left_right(img)
    img = tf.image.random_flip_up_down(img)

    # Perform color augmentation.
    img = tf.image.random_saturation(img, 0.7, 1.3)
    img = tf.image.random_contrast(img, 0.8, 1.2)

    return img_file, img, label

def input_batch_reader(img_files, labels, batch_size, capacity, min_after_dequeue,
                        img_height, img_width, name=None):
    img_files, img, label = labeled_images_reader(img_files, labels, img_height, img_width, name=name)

    img_files_batch, img_batch, label_batch = tf.train.shuffle_batch([img_files, img, label],
                                                   batch_size=batch_size,
                                                   capacity=capacity,
                                                   min_after_dequeue=min_after_dequeue
                                                   )

    return img_batch, label_batch

#########################
### Neural Network Modules.
#########################
def _create_res_module(input, kernel_size, is_training, pipeline_order, feature_maps):
    i = str(pipeline_order)

    conv11 = tf.layers.conv2d(input, feature_maps, kernel_size, strides=1, padding='valid', kernel_initializer=tf.contrib.layers.xavier_initializer(), name='c' + i + '1')
    norm11 = tf.layers.batch_normalization(conv11, axis=-1, training=is_training, name='norm' + i + '1', center=False, scale=False)
    relu11 = tf.nn.relu(conv11, name='r' + i + '1')
    conv12 = tf.layers.conv2d(relu11, feature_maps, kernel_size, strides=1, padding='same', kernel_initializer=tf.contrib.layers.xavier_initializer(), name= 'c' + i + '2')
    conv1 = tf.add(conv11, conv12)
    norm1 = tf.layers.batch_normalization(conv1, axis=-1, training=is_training, name='norm' + i, center=False, scale=False)
    relu1 = tf.nn.relu(conv1, name='r' + i)

    return relu1


class CNN(object):

    _Operations = namedtuple('Operations',
    ['X', 'y', 'loss', 'forward', 'learning_rate',
    'fit', 'gap', 'fcl', 'pred', 'last_feature_maps',
    'cam', 'accuracy', 'accuracy_batch', 'f1', 'precision',
    'recall', 'dropout_prob', 'is_training'])
    operations = \
    _Operations('X', 'y', 'loss', 'forward',
                'learning_rate', 'fit', 'gap',
                'fcl', 'pred', 'last_conv', 'cam',
                'accuracy', 'accuracy_batch', 'f1',
                'precision', 'recall', 'dropout_prob', 'is_training')

    def __init__(self, kernel_size, img_height, img_width, lr=1e-3):
        self.kernel_size = kernel_size
        self.img_height = img_height
        self.img_width = img_width
        self.lr = lr
        self.dropout_training_value = 0.5

        self._class_weights = tf.constant([[0.4, 0.6]])

        self.X = tf.placeholder(np.float32, shape=[None, img_height, img_width, 3], name=self.operations.X)
        self.y = tf.placeholder(np.float32, shape=[None, 2], name=self.operations.y)

        self.training = tf.placeholder_with_default(False, shape=(), name=self.operations.is_training)
        self.dropout_prob = tf.cond(self.training, lambda : self.dropout_training_value, lambda : 1.0)

        self.optimizer = tf.train.AdamOptimizer(learning_rate=self.lr)

        # Initialize the different components right away.
        self.convolutions
        self.forward
        self.predict
        self.loss
        self.gradients
        self.fit
        self.get_last_weights
        self.class_activation_mappings
        self.accuracy
        self.precision
        self.recall
        self.f1_score
        self.scalar_summaries
        self.first_kernels_summary
        self.image_input_summary
        self.weight_histogram_summary
        self.reset_scalar_metrics


    @lazy_property
    def forward(self):

        conv = self.convolutions

        #Global average pooling
        gap = tf.squeeze(tf.nn.pool(conv, conv.shape[1:3], pooling_type='AVG',
                                    padding='VALID',  name=self.operations.gap), axis=[1, 2])
        # flat = tf.reshape(conv, [-1 conv.shape[1].value * conv.shape[2].value * conv.shape[3].value])

        gap = tf.nn.dropout(gap, self.dropout_prob)

        return tf.layers.dense(gap, 2, name=self.operations.fcl)

    @lazy_property
    def predict(self):
        return tf.nn.softmax(self.forward, name=self.operations.pred)

    @lazy_property
    def predict_label(self):
        return tf.squeeze(tf.cast(tf.argmax(self.predict, axis=1), tf.float32)) #tf.squeeze(tf.cast(tf.greater(self.predict, 0.5), tf.float32))

    @lazy_property
    def loss(self):
        logits = self.forward

        # Add weights
        logits = tf.multiply(logits, self._class_weights)

        return tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=logits, labels=self.y), name=self.operations.loss)

    @lazy_property
    def gradients(self):
        return self.optimizer.compute_gradients(self.loss, tf.trainable_variables())

    @lazy_property
    def fit(self):
        return self.optimizer.apply_gradients(self.gradients)#self.optimizer.minimize(self.loss, name=self.operations.fit)

    @lazy_property
    def convolutions(self):
        relu1 = _create_res_module(self.X, self.kernel_size, self.training, 1, 8)
        relu2 = _create_res_module(relu1, self.kernel_size, self.training, 2, 16)
        relu3 = _create_res_module(relu2, self.kernel_size, self.training, 3, 32)
        #relu4 = _create_res_module(relu3, self.kernel_size, self.training, 4, 64)

        return relu3

    @lazy_property
    def get_last_weights(self):
        weights = tf.trainable_variables()[-2] # The last layer weights, biases are not necessary

        return weights

    @lazy_property
    def class_activation_mappings(self):
        conv = self.convolutions
        last_weights = self.get_last_weights

        img_height = conv.shape[1].value
        img_width = conv.shape[2].value
        conv = tf.reshape(conv, [-1,
                                conv.shape[1].value * conv.shape[2].value,
                                conv.shape[3].value])

        def mul_img(img_flat):
            return tf.matmul(img_flat, last_weights)

        cam = tf.map_fn(mul_img, conv, name=self.operations.cam)
        cam = tf.reshape(cam, shape=[-1, img_height, img_width, 1])
        cam = tf.image.resize_images(cam, (self.img_height, self.img_width))

        return cam

    # @lazy_property
    # def accuracy(self):

    @lazy_property
    def precision(self):
        y_pred = self.predict_label
        y = tf.argmax(self.y, axis=1)

        precision, precision_op = tf.metrics.precision(y, y_pred, name=self.operations.precision)

        return precision_op

    @lazy_property
    def recall(self):
        y_pred = self.predict_label

        y = tf.argmax(self.y, axis=1)

        recall, recall_op = tf.metrics.recall(y, y_pred, name=self.operations.recall)

        return recall_op

    @lazy_property
    def f1_score(self):
        y_pred = self.predict_label

        precision = self.precision
        recall = self.recall

        return tf.identity(tf.scalar_mul(2, tf.multiply(precision, recall)) / tf.add(precision, recall),
                   name=self.operations.f1)

    @lazy_property
    def accuracy(self):
        y_pred = self.predict_label

        y = tf.argmax(self.y, axis=1)

        accuracy, accuracy_op = tf.metrics.accuracy(y, y_pred, name=self.operations.accuracy)

        return accuracy_op

    @lazy_property
    def accuracy_batch(self):
        y_pred = self.predict_label

        y = tf.argmax(self.y, axis=1)

        return tf.reduce_mean(
                tf.cast (
                    tf.equal(
                        y, y_pred
                    ), tf.float32),
                 name=self.operations.accuracy_batch)

    # Summary operations
    @lazy_property
    def scalar_summaries(self):
        learning_rate = tf.summary.scalar('learning_rate', self.optimizer._lr)

        scalar_summaries = tf.summary.merge([learning_rate])
        return scalar_summaries

    @lazy_property
    def image_input_summary(self):
        input_images = tf.summary.image('input_images', self.X, max_outputs=3)

        return input_images

    @lazy_property
    def first_kernels_summary(self):
        first_kernels = tf.trainable_variables()[0]
        first_kernels = tf.reshape(first_kernels, (first_kernels.shape[0].value * first_kernels.shape[3].value,
                                    first_kernels.shape[1].value,
                                    first_kernels.shape[2].value, 1)
                                   )

        first_kernels = tf.summary.image('first_layer_weights', first_kernels, max_outputs=24)

        #print(image_summaries)
        return first_kernels

    @lazy_property
    def weight_histogram_summary(self):
        weight_histogram_summaries = []
        for var in tf.trainable_variables():
            weight_histogram_summaries.append( tf.summary.histogram(var.name.replace(':', '_'), var))

        return tf.summary.merge(weight_histogram_summaries)

    @lazy_property
    def gradient_histogram_summary(self):
        gradient_histogram_summaries = []
        for gradient, var in self.gradients:
            gradient_histogram_summaries.append(tf.summary.histogram(var.name.replace(':', '_'), gradient))

        return tf.summary.merge(gradient_histogram_summaries)

    @lazy_property
    def reset_scalar_metrics(self):
        scalar_metrics = [var for var in tf.local_variables() if var.name.split('/')[0] in ['accuracy', 'precision', 'recall', 'f1_score']]

        return tf.variables_initializer(scalar_metrics, name='reset_scalar_metrics')

    def save(self, sess, model_name, global_step=None):
        saver = tf.train.Saver()

        saver.save(sess, model_name, global_step=global_step)


    @staticmethod
    def restore(sess, model_name):
        saver = tf.train.Saver()

        #saver = tf.train.import_meta_graph(model_name + '.meta')
        saver.restore(sess, model_name)

        print('Model {} restored'.format(model_name))


class ConvolutionNINLayer(object):

    def __init__(self, input_tensor, is_training_tensor, feature_maps, kernel_size, id_num):
        self._input_tensor = input_tensor
        self._feature_maps = feature_maps
        self._kernel_size = kernel_size
        self._is_training = is_training_tensor

        self._base_name = 'conv'

        self.nin_convolutions

    @lazy_property
    def nin_convolutions(self):
        conv = tf.layers.conv2d(self._input_tensor, self._feature_maps, self._kernel_size, strides=1, padding='valid', activation=tf.nn.relu, kernel_initializer=tf.contrib.layers.xavier_initializer())
        conv_kernel1 = tf.layers.conv2d(conv, self._feature_maps, 1, strides=1, padding='valid', activation=tf.nn.relu, kernel_initializer=tf.contrib.layers.xavier_initializer())
        normed_conv = tf.layers.batch_normalization(conv_kernel1, axis=1, training=self._is_training)

        return normed_conv

class SlidingWindowModel(object):

    def __init__(self, model_path, kernel_size, window_width, window_height, model=None):
        self._window_width = window_width
        self._window_height = window_height

        self._rpm = RegionProposalModel((self._window_width, self._window_height))
        self._model_path = model_path
        if model is None:
            model = CNN(kernel_size, window_width, window_height)

        self._cnn = model

        self._sess = tf.Session()

        self._cnn.restore(self._sess, model_path)

    @property
    def window_size(self):
        return (self._window_height, self._window_width)

    @property
    def model_path(self):
        return self._model_path

    def predict(self, img, output='img'):
        '''
        If output is \'img\', return the image with drawn bounding boxes.
        If it's \'bboxes\', then the output is (x,y,w,h) where x is the vertical axis.
        '''
        img_width, img_height = img.shape[0], img.shape[1]

        if img_width < self._window_width or img_height < self._window_height:
            raise ValueError('Image size must be bigger than specified window size: {} < {}'.format((img_width, img_height) < (self.window_width, self.window_height)))

        # Copy the image to avoid directly changing it
        img = img.copy()
        bboxes = []

        region_proposals = self._rpm.get_proposals(img)

        # The x axis is vertical
        bboxes = self._run_session(region_proposals, img)

        bboxes = self._non_maximum_suppression(bboxes)

        if output == 'bboxes':
            return bboxes

        if output != 'img':
            raise ValueError('Invalid output mode. Must be either \'img\' or \'bboxes\'.')

        for x,y,w,h,p in bboxes:
            img = self._draw_bounding_box(img, x,y,w,h)

        return img

    def _draw_bounding_box(self, img, x,y,w,h):
        x,y,w,h = int(x), int(y), int(w), int(h)
        return cv2.rectangle(img, (y,x), (y + h, x + w), (255,0,0,0), 5)

    def _run_session(self, input_bboxes, img):
        output_bboxes = []
        sess = self._sess

        i = 1
        for x, y, w, h in input_bboxes:

            window = img[x: x + w, y: y + h]
            try:
                # bboxes_window = self._predict_window(window, sess)
                #
                # bboxes_window = [(x + x_box, y + y_box, w_box, h_box) for x_box, y_box, w_box, h_box in bboxes_window]

                pred, probs = sess.run([self._cnn.predict_label, self._cnn.predict],
                                     feed_dict = {self._cnn.X: window.reshape(1, *window.shape)})

                prob = np.argmax(probs)
                if pred == 1:
                    output_bboxes.append((x,y,w,h,prob))

            except Exception as ex:
                raise ex
                #print(x,y,w,h)
                #print(x,y)

        return output_bboxes

    def _predict_window(self, window, sess):
        pred, cams = sess.run([self._cnn.predict_label, self._cnn.class_activation_mappings],
                             feed_dict = {self._cnn.X: window.reshape(1, *window.shape)})

        bboxes = []
        if pred == 0:
            return bboxes

        cam = cams[0] # Get the class activation mappings

        # Threshold the class activation mappings
        t, cam = cv2.threshold(cam, 0.2 * cam.max(), cam.max(), cv2.THRESH_BINARY)
        cam = cam.astype(np.uint8)

        cam, contours, _ = cv2.findContours(cam, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

        bboxes = [cv2.boundingRect(contour) for contour in contours]

        return bboxes

    def _non_maximum_suppression(self, bboxes):
        bboxes = np.array(bboxes)

        kept_bboxes = []

        # While there are any xy and others left
        while len(bboxes) > 0:
            wh = bboxes[:, 2:4]
            xy1 = bboxes[:, :2]
            xy2 = wh + xy1
            probs = bboxes[:, -1]

            areas = wh.prod(axis=1)

            # Find the highest scoring bbox and store it.
            i = np.argmax(probs)

            kept_bboxes.append(bboxes[i].tolist())

            highest_xy1 = xy1[i]
            highest_xy2 = xy2[i]

            # Find highest top left coordinates and lowest bottom right coordinates of the bounding box
            top_left = np.maximum(xy1, highest_xy1)
            bottom_right = np.minimum(xy2, highest_xy2)

            wh_overlaps = np.maximum(0, bottom_right - top_left)

            # Indexes of boxes that do contain overlap.
            #keep_idxs = (np.abs(highest_xy - xy1) < wh).all(axis=1)

            overlaps = wh_overlaps.prod(axis=1) / areas
            overlaps = overlaps > 0.3

            bboxes = bboxes[~overlaps]

        return kept_bboxes


class RegionProposalModel(object):

    def __init__(self, window_size):
        self._window_width, self._window_height = window_size

    def get_proposals(self, img):
        proposals = self._segment_potential_defects(img)

        proposal_bboxes = get_bboxes(proposals)

        proposal_bboxes = [(x + round(w/2) - round(self._window_width / 2), y + round(h/2) - round(self._window_height / 2),
                            self._window_width,self._window_height)
                            for y,x,h,w in proposal_bboxes]

        proposal_bboxes = [(max(x,0) - max(x + self._window_width - img.shape[0], 0),
                            max(y,0) - max(y + self._window_height - img.shape[1], 0),
                            w,h
                           )
                           for x,y,w,h in proposal_bboxes]

        return proposal_bboxes

    def _segment_potential_defects(self, img):
        gaussian_kernel_size, gaussian_kernel_sigma, adaptive_thresh_kernel_size = \
            15, 1, 7

        blurred = cv2.GaussianBlur(cv2.cvtColor(img, cv2.COLOR_RGB2GRAY),
                                   (gaussian_kernel_size, gaussian_kernel_size), gaussian_kernel_sigma)
        thresh = cv2.adaptiveThreshold(blurred, 255,
                                       cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV,
                                       adaptive_thresh_kernel_size, 2)
        thresh = thresh / 255

        return thresh
