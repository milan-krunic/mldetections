import os
import argparse

from PIL import Image
import numpy as np
import pandas as pd

from util import *

parser = argparse.ArgumentParser()
parser.add_argument('input_data', type=str)
args = parser.parse_args()

input_data = args.input_data
is_folder = os.path.isdir(input_data)

if is_folder:
    img_colors = dict()
    for img_file in os.listdir(input_data):
        if img_file.endswith('.png'):
            img_colors[img_file] = image_colors_number(os.path.join(input_data, img_file))

    bad_files = [img_fl for img_fl, colors in img_colors.items() if colors > 10]
    color_counts = pd.DataFrame(img_colors, index=['Color Counts']).T.reset_index().rename(columns={'index': 'Image'})

    color_counts.to_csv('./color_counts.csv', index=False)
    print('Results saved to color_counts.csv')
else:
    print('Color number: {}'.format(image_colors_number(input_data)))
