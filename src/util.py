import os
import math
import itertools

import pandas as pd
from PIL import Image
import numpy as np
import cv2

COLOR_MAP = {'Add on': (222, 255, 0),
 'Blade Markings': (141, 35, 180),
 'Blade Outline': (211, 199, 219),
 'Chipped Paint': (128, 0, 0),
 'Contamination': (255, 255, 0),
 'Dino Shell': (0, 0, 0),
 'Dino Tail': (255, 128, 64),
 'Drain Hole defects': (255, 128, 0),
 'Edge Delamination': (128, 64, 64),
 'Erosion': (30, 144, 255),
 'Gurney Flap': (128, 255, 255),
 'Healthy Dino Shell': (58, 64, 173),
 'Healthy Dino Tail': (131, 19, 235),
 'Healthy Drain Hole': (47, 177, 74),
 'Healthy Gurney Flap': (204, 156, 8),
 'Healthy LPS': (210, 49, 49),
 'Healthy Power edge': (197, 219, 15),
 'Healthy Stall Strip': (77, 96, 211),
 'Healthy Vortex Generator': (201, 24, 203),
 'Insects or temporary contamination': (18, 178, 6),
 'LPS': (64, 128, 128),
 'Observation': (93, 8, 188),
 'Other Items in a background': (68, 213, 17),
 'Other Turbine in a background': (250, 4, 39),
 'Peeling Paint': (89, 0, 0),
 'Pinholes': (218, 165, 32),
 'Power Edge': (0, 255, 128),
 'Previous Repairs': (198, 230, 70),
 'Same Turbine in a background': (249, 14, 241),
 'Scratches and Gouges': (36, 36, 255),
 'Stall Strip': (255, 0, 128),
 'Structural Cracks': (0, 191, 255),
 'Superficial Longitudinal crack': (255, 69, 0),
 'Superficial Transverse crack': (144, 238, 144),
 'Surface Delamination': (128, 128, 128),
 'Surface Voids': (128, 0, 128),
 'Trailing Edge Laminate Damage': (255, 128, 255),
 'Unknown': (233, 233, 173),
 'Vortex Generator': (0, 64, 64)}

BLADE_LABEL = 'Blade Outline'
PREV_REPAIRS_LABEL = 'Previous Repairs'

non_blade_markings = [
    'Other Items in a background',
    'Other Turbine in a background',
    'Same Turbine in a background']

blade_marking = COLOR_MAP['Blade Outline']

def load_jpg(img_path):
    jpg = Image.open(img_path)
    jpg = np.array(jpg)

    return jpg

def load_png(img_path):
    png = Image.open(img_path)
    png.load()
    img = Image.new('RGB', png.size, (255,255,255))
    img.paste(png, mask=png.split()[3])

    img = np.array(img)

    return img

def load_masked_jpg(img_path, mask_path):
    jpg = Image.open(img_path)
    jpg = np.array(jpg)

    png = load_png(mask_path)

    return mask_jpg(jpg, png)

def mask_jpg(img, mask):
    mask = np.any((mask != [255,255,255]), axis=2)

    return cv2.bitwise_and(img, img, mask=mask.astype(np.int8))

# Refactor two function below
def unique_colors(img):

    # Find better solution?
    return np.array(pd.DataFrame(img.reshape((img.shape[0] * img.shape[1], img.shape[2]))).drop_duplicates())

def image_colors_number(img_path):
    png = load_png(img_path)

    colors = unique_colors(png)

    return colors.shape[0]

def leave_only_blade(img):
    return leave_only_colors(img, [COLOR_MAP[label] for label in COLOR_MAP.keys() if label not in non_blade_markings])

def leave_only_labels(img, labels):
    return leave_only_colors(img, [COLOR_MAP[label] for label in COLOR_MAP.keys() if label in labels])

def leave_only_colors(img, colors):
    rgbs = img.reshape(img.shape[0] * img.shape[1], img.shape[2])
    img_blade = rgbs.copy()

    colors = np.array(colors)
    # colors_hash = [color[0] << 16 + color[1] << 8 + color[2] for color in colors]
    # rgbs_hash = rgbs[:, 0] << 16 + rgbs[:, 1] << 8 + rgbs[:, 2]
    colors_hash = [color[0] * 1e10 + color[1] * 1e5 + color[2] for color in colors]
    rgbs_hash = rgbs[:, 0] * 1e10 + rgbs[:, 1] * 1e5 + rgbs[:, 2]

    img_blade[~(np.in1d(rgbs_hash, colors_hash))] = [255,255,255]

    img_blade = img_blade.reshape(img.shape)

    return img_blade

def uppercase_rename(img_file_path):
    file_path, ext = os.path.splitext(img_file_path)
    name = os.path.basename(file_path)

    name = name.upper()
    path = os.path.dirname(file_path)

    new_file_path = os.path.join(path, name) + ext

    os.rename(img_file_path, new_file_path)

def get_image_pairs(images_folder):
    return ((os.path.join(images_folder, fl),
            os.path.join(images_folder, os.path.splitext(fl)[0] + '.png'))
            for fl in os.listdir(images_folder)
            if fl.endswith('.jpg'))

def get_blade_coordinates(img):
    return get_label_coordinates(img, [BLADE_LABEL], labels=False)

def get_label_coordinates(img, defects, labels=True):
    rgbs = img.reshape(img.shape[0] * img.shape[1], img.shape[2])

    defect_rgbs = [COLOR_MAP[defect] for defect in defects]

    defect_coords = {}
    for defect, defect_rgb in zip(defects, defect_rgbs):
        idxs = np.argwhere((rgbs[:, 0] == defect_rgb[0])
                    & (rgbs[:, 1] == defect_rgb[1])
                    & (rgbs[:, 2] == defect_rgb[2]))
        # idxs = np.argwhere((rgbs == defect_rgb).all(axis=1))

        if idxs.sum() > 0:
            x = (idxs % img.shape[1]).astype(np.int32).ravel().tolist()
            y = np.floor(idxs / img.shape[1]).astype(np.int32).ravel().tolist()

            defect_coords[defect] = np.array(list(zip(x,y)))

    if not labels:
        defect_coords = list(itertools.chain(defect_coords.values()))

    return defect_coords

def get_bboxes(img):
    img = img.astype(np.uint8)
    img, contours, _ = cv2.findContours(img, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

    bboxes = [cv2.boundingRect(contour) for contour in contours]

    return bboxes

def has_defect(img, defects):
    uc = unique_colors(img)

    return contains_label_colors(uc, defects)

# def map_label_coordinates(img):
#     color_to_label_map = {v: k for k,v in COLOR_MAP.items()}
#
#     label_to_coords_map = {}
#     for x in range(img.shape[0]):
#         for y in range(img.shape[1]):
#             color = tuple(img[x,y,:])
#
#             label = color_to_label_map.get(color, None)
#
#             if label is not None:
#                 if label == BLADE_LABEL:
#                     # Check if surrounding pixels are background (meaning that this is an edge pixels)
#                     for x_s, y_s in itertools.product([x - 1, x, x + 1], [y - 1, y, y + 1]):
#                         try:
#                             color_s = tuple(img[x_s, y_s, :])
#
#                             if color_s == (255,255,255):
#                                 label = 'Blade Edge'
#                                 break
#                         except IndexError:
#                             pass
#
#                 label_to_coords_map[label] = (x,y)
#
#     return label_to_coords_map


def contains_label_colors(color_array, label_colors):
    for label_color in label_colors:
        if type(label_color) is str:
            label_color = COLOR_MAP[label_color]

        if (label_color == color_array).all(axis=1).sum() > 0:
            return True

    return False

def extract_window(img, x,y,w,h):
    return img[top_left[1]:top_left[1] + height, top_left[0]:top_left[0] + width, :]

def round_half(number):
    ceil = number - math.floor(number) > 0.5

    if ceil:
        return math.ceil(number)

    return math.floor(number)

def move_to_subfolder(file_path, subfolder):
    cur_dir, name = os.path.split(file_path)
    new_file_path = os.path.join(cur_dir, subfolder, name)

    os.rename(file_path, new_file_path)
