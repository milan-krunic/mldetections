import inspect
import functools

import tensorflow as tf

def lazy_property(function):
    function_name = function.__name__
    attribute = '_' + function_name

    name_scope = tf.get_default_graph().get_name_scope()
    parent_scope = name_scope.split('/')[-1]

    @property
    @functools.wraps(function)
    def decorator(self):
        if not hasattr(self, attribute):
            with tf.variable_scope(function_name):
                setattr(self, attribute, function(self))

        return getattr(self, attribute)

    return decorator
