import os
import argparse
import matplotlib.pyplot as plt

from PIL import Image
import numpy as np
import tqdm
import cv2

from util import *

parser = argparse.ArgumentParser()
parser.add_argument('input_folder', type=str)
parser.add_argument('output_folder', type=str)
args = parser.parse_args()

input_folder = args.input_folder
output_folder = args.output_folder

if not os.path.exists(output_folder):
    os.mkdir(output_folder)

erronous_files = []
missing_files = []
for png_file in tqdm.tqdm([file_name for file_name in os.listdir(input_folder) if file_name.endswith('.png')]):
    jpg_file = os.path.join(input_folder, png_file.split('.')[0] + '.jpg')

    if not os.path.exists(jpg_file):
        missing_files.append(jpg_file)
        continue

    img = load_jpg(os.path.join(input_folder, jpg_file))

    img_mask = load_png(os.path.join(input_folder, png_file))
    img_blade_mask = leave_only_blade(img_mask)

    # Refactor error handling.
    try:
        img_blade = mask_jpg(img, img_blade_mask)

        plt.imsave(os.path.join(output_folder, png_file.split('.')[0] + '.jpg'), img_blade, format='jpg', dpi=1)
        plt.imsave(os.path.join(output_folder, png_file), img_blade_mask, format='png', dpi=1)
    except (FileNotFoundError, FileExistsError) as ex:
        raise ex
    except Exception as ex:
        erronous_files.append(png_file)

import pickle

with open('erronous_files.txt', 'w+') as fl:
    fl.write(str(erronous_files))

with open('missing_files.txt', 'w+') as fl:
    fl.write(str(missing_files))
