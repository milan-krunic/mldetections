import numpy as np
import cv2
import imutils

from util import *

window_width, window_height, wanted_defects = 100, 100, ['Surface Voids', 'Peeling Paint', 'Chipped Paint']

# Augmentation parameters
x_translations_range = [-round(window_width/4), round(window_width/4)]
y_translations_range = [-round(window_height/4), round(window_height/4)]
resize_range = [1/2, 2]
flip_choices = [0, 1, -1]
color_perturbation = None

def choose_random_window_coord(coords, window_width, window_height, img_size):
    try:
        x, y = coords[np.random.randint(len(coords))]
    except TypeError:
        print(coords)
        raise AssertionError()

    return x, y

def translate_coordinates(x, y, x_trans, y_trans, img_size):
    x = x + x_trans

    if x < 0 or x > img_size[1]:
        x = x - 2*x_trans

    y = y + y_trans

    if y < 0 or y > img_size[0]:
        y = y - 2*y_trans

    return x,y

def sample_window_by_coords(jpg, png, label_coords, window_width, window_height, translate=True):
    # Choose a random coordinate to be the window center.
    x,y = choose_random_window_coord(label_coords, window_width, window_height, png.shape)

    # Perform random translations.
    if translate:
        x_translate_factor = round(np.random.uniform(*x_translations_range))
        y_translate_factor = round(np.random.uniform(*y_translations_range))

        x, y = translate_coordinates(x, y, x_translate_factor, y_translate_factor, png.shape)

    # Fix if the window is out of the image.
    width_half = round(window_width / 2)
    height_half = round(window_height / 2)

    x = x + max(width_half - x, 0) + min(png.shape[1] - (x + width_half), 0)
    y = y + max(height_half - y, 0) + min(png.shape[0] - (y + height_half), 0)

    x, y = (x - width_half, y - height_half)

    # Remove the coordinates within the window to not be sampled again.
    label_coords = label_coords[~((label_coords[:, 0] >= x)
    & (label_coords[:, 0] < x + window_width)
    & (label_coords[:, 1] >= y)
    & (label_coords[:, 1] < y + window_height)), :]

    window = extract_window(jpg, (x,y), window_width, window_height)

    short_label = has_defect(extract_window(png, (x,y), window_width, window_height), wanted_defects) # Do a double check to ensure the correctness of the data after translations.

    return window, short_label, label_coords

def get_blade_edge_coordinates(png):
    # Get only blade outline annotations
    blade_outline = (png == COLOR_MAP[BLADE_LABEL]).all(axis=2).astype(np.uint8)

    # Fill the blade outline
    structuringEl = cv2.getStructuringElement(cv2.MORPH_RECT, (100,100))
    blade_outline = cv2.morphologyEx(blade_outline, cv2.MORPH_CLOSE, structuringEl)

    # Get the contours of the filled figure, which corresepond to the edges of the blade outline.
    _, edges, _ = cv2.findContours(blade_outline, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

    edges = max(edges, key=lambda x: len(x))

    edge_coordinates = edges.squeeze()

    return edge_coordinates

def sample_windows(jpg, png):
    # Resize input image.
    resize_factor = np.random.uniform(*resize_range)

    jpg = cv2.resize(jpg, None, fx=resize_factor, fy=resize_factor, interpolation=cv2.INTER_LINEAR)
    png = cv2.resize(png, None, fx=resize_factor, fy=resize_factor, interpolation=cv2.INTER_NEAREST) # Nearest neighbour interpolaion to keep the colors.

    other_labels = [k for k in COLOR_MAP.keys() if k not in wanted_defects
                                                    and k != PREV_REPAIRS_LABEL
                    ]

    coordinate_map = get_label_coordinates(png, COLOR_MAP.keys())
    try:
        blade_coordinates = coordinate_map[BLADE_LABEL]
    except KeyError:
        raise ValueError('No blade was found on given image.')
    del coordinate_map[BLADE_LABEL]

    windows = []
    short_labels = []
    full_labels = []

    # Sample the defect windows. Sample all possible windows, and keep the count of how many are sampled.
    sampled_defects = 0 # Count of how many defect windows are sampled. Used later for sampling blade regions.
    for wanted_defect in wanted_defects:
        if wanted_defect in coordinate_map:
            defect_coords = coordinate_map[wanted_defect]

            while len(defect_coords) > 0:
                window, short_label, defect_coords = sample_window_by_coords(jpg, png, defect_coords, window_width, window_height)

                windows.append(window)
                short_labels.append(short_label)
                full_labels.append(wanted_defect)

                sampled_defects += 1

    # Sample other labels. Sample one window per label, but don't keep the count of how many are sampled.
    for other_label in other_labels:
        if other_label in coordinate_map:
            label_coords = coordinate_map[other_label]

            if len(label_coords) == 0:
                break

            window, short_label, label_coords = sample_window_by_coords(jpg, png, label_coords, window_width, window_height)

            windows.append(window)
            short_labels.append(short_label)
            full_labels.append(other_label)

    # Sample blade edges. First, use edge detection on the blade outline annotations to locate the edges,
    # then sample the half of the sampled defects number of edges.
    blade_edge_coords = get_blade_edge_coordinates(png)
    for i in range(math.floor(sampled_defects/2)):
        if len(blade_edge_coords) == 0:
            break

        if len(blade_edge_coords.shape) == 1 or blade_edge_coords.shape[1] == 1:
            break

        window, short_label, label_coords = sample_window_by_coords(jpg, png, blade_edge_coords, window_width, window_height, translate=False)

        windows.append(window)
        short_labels.append(short_label)
        full_labels.append('Blade Edge')

    # Sample only blade surface. This may sample other things as well, but it should not make a big difference, since it's much more likely this will be a clear blade.
    for i in range(math.ceil(sampled_defects/2)):
        if len(blade_coordinates) == 0:
            break

        window, short_label, label_coords = sample_window_by_coords(jpg, png, blade_coordinates, window_width, window_height)
        windows.append(window)
        short_labels.append(short_label)
        full_labels.append('Blade')

    return windows, short_labels, full_labels
