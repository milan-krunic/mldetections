import argparse
import tqdm

from util import *

# We will assume that any image will not have more than 50% percent of all finding markings on it.
color_thresh = len(COLOR_MAP) * 0.5

response = ''
try:
    while response.lower() != 'y' and response.lower() != 'n':
        response = input('Be careful, calling this script can remove some images. Continue (Y/N)?')
except (EOFError, KeyboardInterrupt):
    pass

if response.lower() == 'y':

    parser = argparse.ArgumentParser()
    parser.add_argument('input_folder', type=str)
    args = parser.parse_args()

    input_folder = args.input_folder

    removed_files = 0
    for jpg_file, png_file in tqdm.tqdm(list(get_image_pairs(input_folder))):
        color_counts = image_colors_number(png_file)

        if color_counts > color_thresh:
            os.remove(jpg_file)
            os.remove(png_file)

            removed_files += 1

    print('Removed {} files'.format(removed_files))
