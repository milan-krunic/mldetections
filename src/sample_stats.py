import os
import tqdm

from util import *

color_to_label = {color: label for label, color in COLOR_MAP.items() if label != BLADE_LABEL}
defect_colors = list(color_to_label.keys())

input_path = '/media/aleksandar/Data/Downloads/A'
output_path = '/media/aleksandar/Data/Downloads/A'

img_names = []
img_defects = []
for _, png_file in tqdm.tqdm(list(get_image_pairs(input_path))):
    png = load_png(png_file)

    file_name = os.path.basename(png_file).split('.')[0]

    uc = unique_colors(png)

    defects = [color_to_label[tuple(color)] for color in uc if tuple(color) in color_to_label]

    img_names.append(file_name)
    img_defects.append(defects)

list_data = pd.DataFrame({'Name': img_names, 'Defects': img_defects})
list_data = list_data[['Name', 'Defects']]

list_data.to_csv(os.path.join(output_path, 'list_data.csv'), index=False)


defect_counts = {}
for i, defects in list_data.Defects.iteritems():
    for defect in defects:
        defect_counts[defect] = defect_counts.get(defect, 0) + 1

defect_counts = pd.DataFrame(defect_counts, index=['Count']).T
defect_counts.to_csv(os.path.join(output_path, 'defect_counts.csv'), index=True)
