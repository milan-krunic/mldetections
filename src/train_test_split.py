import os
import argparse

from sklearn.model_selection import train_test_split

from util import *

parser = argparse.ArgumentParser()
parser.add_argument('input_folder', type=str)
parser.add_argument('test_perc',  type=float)

args = parser.parse_args()
input_folder = args.input_folder
test_perc = args.test_perc

img_files = list(get_image_pairs(input_folder))

# Randomly
train, test = train_test_split(img_files, test_size=test_perc)

try:
    os.mkdir(os.path.join(input_folder, 'train'))
except FileExistsError:
    pass

try:
    os.mkdir(os.path.join(input_folder, 'test'))
except FileExistsError:
    pass


for train_jpg, train_png in train:
    move_to_subfolder(train_jpg, 'train')
    if os.path.exists(train_png):
        move_to_subfolder(train_png, 'train')


for test_jpg, test_png in test:
    move_to_subfolder(test_jpg, 'test')
    if os.path.exists(train_png):
        move_to_subfolder(test_png, 'test')
