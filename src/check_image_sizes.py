import tqdm
import argparse
import os

from PIL import Image
import numpy as np

parser = argparse.ArgumentParser()

parser.add_argument('images_folder', type=str)
args = parser.parse_args()
images_folder = args.images_folder

img_list = [img_file.split('.')[0] for img_file in os.listdir(images_folder) if img_file.endswith('jpg')]

jpg_shapes = list()
png_shapes = list()
for img_file in tqdm.tqdm(img_list):

    jpg = Image.open(os.path.join(images_folder, img_file + '.jpg'))
    png = Image.open(os.path.join(images_folder, img_file + '.png'))

    jpg_shapes.append(jpg.size)
    png_shapes.append(png.size)

img_list = np.array(img_list)
jpg_shapes = np.array(jpg_shapes)
png_shapes = np.array(png_shapes)

diff_imgs = img_list[np.any(jpg_shapes != png_shapes, axis=1)]
diff_number = diff_imgs.size

print('{} images have inconsistent sizes between png and jpg formats.'.format(diff_number))

print(diff_imgs)
