import tqdm
import argparse
import os

from PIL import Image
import numpy as np

from util import *

parser = argparse.ArgumentParser()

parser.add_argument('images_folder', type=str)
args = parser.parse_args()
images_folder = args.images_folder

img_list = [img_file for img_file in os.listdir(images_folder)]

for img_file in tqdm.tqdm(img_list):
    img_path = os.path.join(images_folder, img_file)

    uppercase_rename(img_path)
